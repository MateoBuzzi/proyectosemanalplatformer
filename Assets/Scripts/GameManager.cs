﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool gameOver = false;

    public static bool winCondition = false;

    public static int actualPlayer = 0;

    public List<Controller_Target> targets;

    public List<Controller_Player> players;

    public float furia = 0;
    public Rigidbody rb;

    public BoxCollider col;

    public GameObject jugador0;
    public GameObject jugador1;
    public GameObject jugador2;
    public GameObject jugador3;
    public GameObject jugador4;
    public GameObject jugador5;

    void Start()
    {
        Physics.gravity = new Vector3(0, -30, 0);
        gameOver = false;
        winCondition = false;
        SetConstraits();
    }

    void Update()
    {
        GetInput();
        CheckWin();

        

    }

    private void CheckWin()
    {
        int i = 0;
        foreach(Controller_Target t in targets)
        {
            if (t.playerOnTarget)
            {
                i++;
                //Debug.Log(i.ToString());
            }
        }
        if (i >= 7)
        {
            winCondition = true;
        }
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && furia < 100)
        {
            jugador1.SetActive(true);
            jugador0.SetActive(false);
            jugador2.SetActive(false);
            jugador3.SetActive(false);
            jugador4.SetActive(false);
            jugador5.SetActive(false);
            SetConstraits();
            actualPlayer = 1;
                


        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && furia < 100)
        {
            jugador1.SetActive(false);
            jugador0.SetActive(false);
            jugador2.SetActive(true);
            jugador3.SetActive(false);
            jugador4.SetActive(false);
            jugador5.SetActive(false);
            SetConstraits();
            actualPlayer = 2;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && furia < 100)
        {
            jugador1.SetActive(false);
            jugador0.SetActive(false);
            jugador2.SetActive(false);
            jugador3.SetActive(true);
            jugador4.SetActive(false);
            jugador5.SetActive(false);
            SetConstraits();
            actualPlayer = 3;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && furia < 100)
        {
            jugador1.SetActive(false);
            jugador0.SetActive(true);
            jugador2.SetActive(false);
            jugador3.SetActive(false);
            jugador4.SetActive(false);
            jugador5.SetActive(false);
            SetConstraits();
            actualPlayer = 0;
        }
        if (Input.GetKeyDown(KeyCode.Space) && furia < 100)
        {
            jugador1.SetActive(false);
            jugador0.SetActive(false);
            jugador2.SetActive(false);
            jugador3.SetActive(false);
            jugador4.SetActive(true);
            jugador5.SetActive(false);
            SetConstraits();
            actualPlayer = 4;
        }
    }
    private void SetConstraits()
    {
        foreach(Controller_Player p in players)
        {
            if (p == players[actualPlayer])
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
        }
    }
}
