﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrulla : MonoBehaviour
{
    private Rigidbody Movimiento;
    public float speed = 2;
    
    void Start()
    {
        Movimiento = GetComponent<Rigidbody>();
        Movimiento.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
    }

    
    void Update()

    {
        Movimiento.velocity = new Vector2(speed, Movimiento.velocity.y);
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag=="Floor")
        {
            speed *= -1;
            this.transform.rotation = new Quaternion(this.transform.rotation.x * -1,this.transform.rotation.y,this.transform.rotation.z,this.transform.rotation.w);

        }
    }
}
