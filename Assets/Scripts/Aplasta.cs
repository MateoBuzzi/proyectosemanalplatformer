﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aplasta : MonoBehaviour
{
    public Transform Player;
    public Transform enemigo;
    public float speed;

    private bool activo;
    private Vector3 PlayerPosicion;

    private void Update()
    {
        PlayerPosicion = new Vector3(enemigo.position.x, Player.position.y, enemigo.position.z);

        if (activo == true)
        {
            enemigo.transform.position = Vector3.MoveTowards(transform.position, PlayerPosicion, speed * Time.deltaTime);

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {

            activo = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {

            activo = false;
        }
    }












}